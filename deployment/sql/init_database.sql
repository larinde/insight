DROP DATABASE workflowengine;
DROP ROLE  workflowengineuser;

CREATE ROLE  workflowengineuser login password 'workflowenginepass';

CREATE DATABASE workflowengine WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'English_United States.1252' LC_CTYPE = 'English_United States.1252';

GRANT ALL ON workflowengine TO workflowengineuser;

ALTER DATABASE workflowengine OWNER TO workflowengineuser;



