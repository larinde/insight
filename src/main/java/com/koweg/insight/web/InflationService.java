package com.koweg.insight.web;

import java.math.BigDecimal;

public interface InflationService {
    InflationEstimate estimateInflation(String offerDate, BigDecimal offerPrice, BigDecimal percentage);
}
