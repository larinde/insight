package com.koweg.insight.web;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.UUID;


@RestController
@RequestMapping("/api/securities/evaluation")
public class PricingResource {
  private final InflationService inflationService;

  public PricingResource(InflationService inflationService) {
    this.inflationService = inflationService;
  }

  @GetMapping(value = "/inflation/offerPrice/{offerPrice}/offerDate/{offerDate}/percentage/{percentage}", produces = {MediaType.APPLICATION_JSON_VALUE})
  public InflationEstimate inflatedCost(@PathVariable BigDecimal offerPrice, @PathVariable String offerDate, @PathVariable BigDecimal percentage){
    return  inflationService.estimateInflation(offerDate, offerPrice, percentage);
  }

}