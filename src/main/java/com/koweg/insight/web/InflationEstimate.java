package com.koweg.insight.web;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
public class InflationEstimate {
  private final BigDecimal addedCost;
}
