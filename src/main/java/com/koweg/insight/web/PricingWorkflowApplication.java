package com.koweg.insight.web;

import org.camunda.bpm.spring.boot.starter.annotation.EnableProcessApplication;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableProcessApplication
public class PricingWorkflowApplication {

	public static void main(String[] args) {
		SpringApplication.run(PricingWorkflowApplication.class, args);
	}

}
