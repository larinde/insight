package com.koweg.insight.web;

import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.dmn.api.core.*;
import org.kie.dmn.core.api.DMNFactory;
import org.kie.dmn.core.compiler.RuntimeTypeCheckOption;
import org.kie.dmn.core.impl.DMNRuntimeImpl;
import org.kie.dmn.core.util.KieHelper;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.UUID;

@Service
public class InflationServiceImpl implements InflationService{
    private static final String MODEL_ID = "_79C9820B-8D73-4416-82D0-7025AE0B46B7";
    private static final String MODEL_FILENAME = "pricing.dmn";
    private static final String MODEL_NAMESPACE = "https://kiegroup.org/dmn/_53F27F4F-76EC-4D5E-BE34-35565AC0B7E1";
    private static final String MODEL_NAME = "pricing";

    @Override
    public InflationEstimate estimateInflation(String offerDate, BigDecimal offerPrice, BigDecimal percentage) {
        DMNRuntime runtime = createRuntime(MODEL_FILENAME, this.getClass());
        final DMNModel dmnModel = runtime.getModel(MODEL_NAMESPACE, MODEL_NAME );
        final DMNContext dmnContext = DMNFactory.newContext();
        dmnContext.set(InflationModel.INPUT_OFFER_PRICE, offerPrice);
        dmnContext.set(InflationModel.INPUT_ORDER_DATE, offerDate);
        dmnContext.set(InflationModel.INPUT_INFLATION_RATE, percentage);

        final DMNResult result = runtime.evaluateAll(dmnModel, dmnContext);

        return new InflationEstimate(new BigDecimal(String.valueOf(result.getContext().get(InflationModel.RESULT_INFLATION_DECISION))));
    }

    private static DMNRuntime createRuntime(final String resourceName, final Class testClass) {
        final KieServices ks = KieServices.Factory.get();
        final KieContainer kieContainer = KieHelper.getKieContainer(
                ks.newReleaseId("org.kie", "dmn-test-"+ UUID.randomUUID(), "1.0"),
                ks.getResources().newClassPathResource(resourceName, testClass));
        DMNRuntime runtime = kieContainer.newKieSession().getKieRuntime(DMNRuntime.class);
        ((DMNRuntimeImpl) runtime).setOption(new RuntimeTypeCheckOption(true));
        return runtime;
    }

}
