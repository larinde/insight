package com.koweg.insight.web;

public interface InflationModel {
    String INPUT_OFFER_PRICE = "offerPrice";
    String INPUT_ORDER_DATE = "orderDate";
    String INPUT_INFLATION_RATE = "percentage";
    String RESULT_INFLATION_DECISION = "InflationDecision";
}
